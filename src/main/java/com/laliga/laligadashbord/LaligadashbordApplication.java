package com.laliga.laligadashbord;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LaligadashbordApplication {

	public static void main(String[] args) {
		SpringApplication.run(LaligadashbordApplication.class, args);
	}

}
