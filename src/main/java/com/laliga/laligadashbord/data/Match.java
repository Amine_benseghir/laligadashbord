package com.laliga.laligadashbord.data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
public class Match {


    @Id
    private long id;
    private String season;
    private String date;
    private String homeTeam;
    private String awayTeam;
    private String fthg;
    private String ftag;
    private String ftr;
    private String hthg;
    private String htag;
    private String htr;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(String homeTeam) {
        this.homeTeam = homeTeam;
    }

    public String getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(String awayTeam) {
        this.awayTeam = awayTeam;
    }

    public String getFthg() {
        return fthg;
    }

    public void setFthg(String fthg) {
        this.fthg = fthg;
    }

    public String getFtag() {
        return ftag;
    }

    public void setFtag(String ftag) {
        this.ftag = ftag;
    }

    public String getFtr() {
        return ftr;
    }

    public void setFtr(String ftr) {
        this.ftr = ftr;
    }

    public String getHthg() {
        return hthg;
    }

    public void setHthg(String hthg) {
        this.hthg = hthg;
    }

    public String getHtag() {
        return htag;
    }

    public void setHtag(String htag) {
        this.htag = htag;
    }

    public String getHtr() {
        return htr;
    }

    public void setHtr(String htr) {
        this.htr = htr;
    }
}
