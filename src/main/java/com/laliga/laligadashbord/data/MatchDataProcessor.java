package com.laliga.laligadashbord.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import java.time.LocalDate;
import java.util.Date;

public class MatchDataProcessor implements ItemProcessor<MatchInput,Match> {

    private static final Logger log = LoggerFactory.getLogger(MatchDataProcessor.class);

    @Override
    public Match process(MatchInput matchInput) throws Exception {
        Match match = new Match();
        match.setId(Long.parseLong(matchInput.getId()));
        match.setSeason(matchInput.getSeason());
        match.setDate(matchInput.getDate());
        match.setHomeTeam(matchInput.getHomeTeam());
        match.setAwayTeam(matchInput.getAwayTeam());
        match.setFthg(matchInput.getFTHG());
        match.setFtr(matchInput.getFTR());
        match.setHtag(matchInput.getHTAG());
        match.setHtr(matchInput.getHTR());
        return match;
    }
}
