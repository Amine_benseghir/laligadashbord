package com.laliga.laligadashbord.data;

public class MatchInput {

    private String Id;
    private String Season;
    private String Date;
    private String HomeTeam;
    private String AwayTeam;
    private String FTHG;
    private String FTAG;
    private String FTR;
    private String HTHG;
    private String HTAG;
    private String HTR;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        this.Id = id;
    }

    public String getSeason() {
        return Season;
    }

    public void setSeason(String season) {
        Season = season;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getHomeTeam() {
        return HomeTeam;
    }

    public void setHomeTeam(String homeTeam) {
        HomeTeam = homeTeam;
    }

    public String getAwayTeam() {
        return AwayTeam;
    }

    public void setAwayTeam(String awayTeam) {
        AwayTeam = awayTeam;
    }

    public String getFTHG() {
        return FTHG;
    }

    public void setFTHG(String FTHG) {
        this.FTHG = FTHG;
    }

    public String getFTAG() {
        return FTAG;
    }

    public void setFTAG(String FTAG) {
        this.FTAG = FTAG;
    }

    public String getFTR() {
        return FTR;
    }

    public void setFTR(String FTR) {
        this.FTR = FTR;
    }

    public String getHTHG() {
        return HTHG;
    }

    public void setHTHG(String HTHG) {
        this.HTHG = HTHG;
    }

    public String getHTAG() {
        return HTAG;
    }

    public void setHTAG(String HTAG) {
        this.HTAG = HTAG;
    }

    public String getHTR() {
        return HTR;
    }

    public void setHTR(String HTR) {
        this.HTR = HTR;
    }
}
