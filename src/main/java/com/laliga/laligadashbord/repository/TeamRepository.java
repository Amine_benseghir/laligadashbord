package com.laliga.laligadashbord.repository;

import com.laliga.laligadashbord.model.Team;
import org.springframework.data.repository.CrudRepository;

public interface TeamRepository extends CrudRepository<Team,Long> {

    Team findByTeamName(String teamName);

}
